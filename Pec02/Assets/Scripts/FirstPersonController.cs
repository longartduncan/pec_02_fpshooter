using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;


public class FirstPersonController : MonoBehaviour
{
    [SerializeField] private float m_WalkSpeed = 5.0f;
    [SerializeField] private float m_RunSpeed = 10.0f;
    [SerializeField] private float m_AirSpeedMultiplier = 1.0f;
    [SerializeField] private float m_Gravity = 10.0f;
    [SerializeField] private float m_GravityMultiplier = 20.0f;
    [SerializeField] private float m_MouseSensitivity = 2.0f;

    private CharacterController m_CharacterController;
    public bool canMove = true;

    private Camera m_Camera;
    private float m_VerticalRotation = 0;
    private Vector3 m_MoveDirection = Vector3.zero;

    private Rigidbody rb;
    
    private bool m_IsFalling = false;

    private HealthManager healthManager;
    private UIManager uiManager;
    public SceneLoader sceneLoader;

    private NavMeshAgent agent;

    public bool haveKeyOne = false;
    public bool haveKeyTwo = false;

    public bool haveTouchButtonOne = false;

    


    public void Start()
    {
        sceneLoader= GetComponent<SceneLoader>();
        healthManager = GetComponent<HealthManager>();
        uiManager= GetComponent<UIManager>();
        m_CharacterController = GetComponent<CharacterController>();
        m_Camera = GetComponentInChildren<Camera>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        rb=GetComponent<Rigidbody>();
        agent= GetComponent<NavMeshAgent>();
        
    }

    public void Update()
    {
        if (canMove && !healthManager.IsDead)
        {
            // Movimiento
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            Vector3 movement = transform.forward * moveVertical + transform.right * moveHorizontal;
            movement.Normalize();
            movement *= Input.GetKey(KeyCode.LeftShift) ? m_RunSpeed : m_WalkSpeed;

            m_MoveDirection.y -= m_Gravity * Time.deltaTime;
            m_CharacterController.Move(m_MoveDirection * Time.deltaTime);

            // Rotaci�n
            float rotateHorizontal = Input.GetAxis("Mouse X") * m_MouseSensitivity;
            float rotateVertical = Input.GetAxis("Mouse Y") * m_MouseSensitivity;

            transform.Rotate(0, rotateHorizontal, 0);
            m_VerticalRotation -= rotateVertical;
            m_VerticalRotation = Mathf.Clamp(m_VerticalRotation, -90f, 90f);
            m_Camera.transform.localRotation = Quaternion.Euler(m_VerticalRotation, 0, 0);

            //Caida
            if (m_IsFalling)
            {
                rb.AddForce(Vector3.down * m_GravityMultiplier, ForceMode.Acceleration);
            }

           
                // Tocar suelo
                if (m_CharacterController.isGrounded)
            {
                m_MoveDirection = movement;
                agent.enabled = true;
            }
            else
            {
                m_MoveDirection = movement * m_AirSpeedMultiplier;
                m_MoveDirection.y -= m_GravityMultiplier * Time.deltaTime;
                agent.enabled = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
                uiManager.gamePauseImage.SetActive(true);
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                canMove = false;
            }
            else
            {
                ResumeGame();
            }
        }

        if (Input.GetKeyDown(KeyCode.Z) && haveTouchButtonOne)
        {
            Debug.Log("He puesto la llave");
        }

    }

    public void ResumeMovement()
    {
        canMove = true;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        uiManager.gamePauseImage.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        ResumeMovement();
    }

    public void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.tag == "Enemy")
        {
            healthManager.OnHitByEnemy();
            Debug.Log("Me ha herido un enemigo");
        }

        if (hit.gameObject.tag == "BulletEnemy")
        {
            healthManager.OnHitByEnemy();
            Debug.Log("Me ha disparado un enemigo");
        }

        if (hit.gameObject.tag == "Heal")
        {
            healthManager.OnPickUpHealthItem();
            Debug.Log("He recuperado salud");
        }

        if (hit.gameObject.tag == "Shield")
        {
            healthManager.OnPickUpShieldItem();
            Debug.Log("He recuperado escudo");
        }

        if (haveKeyOne && hit.gameObject.tag == "DoorOne")
        {
            Debug.Log("La puerta esta abierta!");
            Transform doorTransform = hit.gameObject.GetComponent<Transform>();
            doorTransform.position = new Vector3(doorTransform.position.x, doorTransform.position.y + 7f, doorTransform.position.z);
        }

        if (hit.gameObject.tag == "DoorTwo")
        {
            Debug.Log("La puerta esta abierta!");
            Transform doorTransform = hit.gameObject.GetComponent<Transform>();
            doorTransform.position = new Vector3(doorTransform.position.x, doorTransform.position.y + 7f, doorTransform.position.z);
        }

        

        Rigidbody body = hit.collider.attachedRigidbody;

        
        if (body == null || body.isKinematic)
            return;

        body.AddForceAtPosition(m_CharacterController.velocity,
                                hit.point,
                                ForceMode.Impulse);
    }

    
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("KeyOne"))
        {
            haveKeyOne = true;
            Debug.Log("He tomado la llave");
            Destroy(other.gameObject);
        }
        if (other.CompareTag("KeyTwo"))
        {
            haveKeyTwo = true;
            Debug.Log("He tomado la llave");
            Destroy(other.gameObject);
        }

        if (other.CompareTag("ButtonOne"))
        {
            haveTouchButtonOne = true;
            Debug.Log("He tocado el boton");
            Destroy(other.gameObject);
        }

        if (other.CompareTag("ObstacleOne") && haveTouchButtonOne)
        {
            
            Destroy(other.gameObject);
            Debug.Log("Se ha quitado el obstaculo!");
        }

        if (other.CompareTag("Finish"))
        {
            Time.timeScale = 0;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            //haveKeyOne = true;
            Debug.Log("He ganado");
            uiManager.GameWin();
            Destroy(other.gameObject);
            canMove = false;
        }

        if (other.CompareTag("LevelInterior"))
        {
            Debug.Log("Paso al siguiente nivel");
            NextLevelInterior();
        }

        if (other.CompareTag("LevelExterior"))
        {
            Debug.Log("Paso al siguiente nivel");
            NextLevelExterior();
        }
    }

    public void NextLevelInterior()
    {
        SceneManager.LoadScene("Level_2");
    }

    public void NextLevelExterior()
    {
       
            SceneManager.LoadScene("Level_1_B");
       
    }


}


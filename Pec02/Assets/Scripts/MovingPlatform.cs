using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public float moveSpeed = 5f; 
    public List<Transform> movePoints; 
    public int currentPoint = 0; 
    public bool isActivePlatform = true; 
    FirstPersonController firstPersonController;

    public void Start()
    {
        firstPersonController = GetComponent<FirstPersonController>();
   
    }

    void Update()
    {
        if (isActivePlatform) 
        {
            
            if (movePoints.Count > 0)
            {
                
                transform.position = Vector3.MoveTowards(transform.position, movePoints[currentPoint].position, moveSpeed * Time.deltaTime);

                
                if (transform.position == movePoints[currentPoint].position)
                {
                    currentPoint = (currentPoint + 1) % movePoints.Count; 
                }
            }
        }
        
    }

      

    

}
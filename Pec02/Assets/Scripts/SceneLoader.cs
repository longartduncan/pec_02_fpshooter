using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    //MENU
    public GameObject menuButtonImage;
    public GameObject playButtonImage;
    public GameObject settingsButtonImage;
    public GameObject creditsButtonImage;
    public GameObject exitButtonImage;

    

    private void Start()
    {
        playButtonImage.SetActive(false);
        settingsButtonImage.SetActive(false);
        creditsButtonImage.SetActive(false);
        exitButtonImage.SetActive(false);
    }
    //MENU
    public void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    //Play
    public void Play()
    {
        playButtonImage.SetActive(true);
    }

    public void ContinueLoadLevel()
    {
        SceneManager.LoadScene("Level_1_A");
    }

    public void NewLoadLevel()
    {
        SceneManager.LoadScene("Level_1_A");        
    }

    //Settings
    public void Settings()
    {
        settingsButtonImage.SetActive(true);
    }
    public void noSettings()
    {
        settingsButtonImage.SetActive(false);
    }

    //Credits
    public void Credits()
    {
        creditsButtonImage.SetActive(true);
    }
    public void noCredits()
    {
        creditsButtonImage.SetActive(false);
    }


    //Exit
    public void Exit()
    {
        exitButtonImage.SetActive(true);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    public void noExit()
    {
        exitButtonImage.SetActive(false);
    }

}

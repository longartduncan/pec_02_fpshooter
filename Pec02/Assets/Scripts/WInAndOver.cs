using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WInAndOver : MonoBehaviour
{
    public void Again()
    {
        SceneManager.LoadScene("Level_1_A");
    }

    public void Exit()
    {
        SceneManager.LoadScene("Menu");
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class enemyAI : MonoBehaviour
{
    [HideInInspector] public PatrolState patrolState;
    [HideInInspector] public AlertState alertState;
    [HideInInspector] public AttackState attackState;
    [HideInInspector] public IEnemyState currentState;

    [HideInInspector] public NavMeshAgent navMeshAgent;

    public Light myLight;
    public float life = 100;
    public float timeBetweenShoots = 1.0f;
    public float damageForce = 10f;
    public float rotationTime = 7.0f;
    public float shootHeight = 0.5f;
    public Transform[] wayPoints;

    public GameObject bulletGunEnemy;
    private GameObject clonBulletGunEnemy;
    public GameObject creatorBulletGunEnemy;
    public float forceGunEnemy;

    public HealthManager healthManager;

    private void Start()
    {
        //Dos estados de AI
        patrolState = new PatrolState(this);
        alertState = new AlertState(this);
        attackState = new AttackState(this);

        currentState = patrolState;

        navMeshAgent = GetComponent<NavMeshAgent>();
        healthManager = GetComponent<HealthManager>();

    }

    private void Update()
    {
        currentState.UpdateState();

        if (life < 0) Destroy(this.gameObject);
    }

    public void Hit(float damage)
    {
        life -= damage;
        currentState.Impact();
        Debug.Log("Enemy hitted: " + life);
    }

    private void OnTriggerEnter(Collider col)
    {
        currentState.OnTriggerEnter(col);
        
    }

    private void OnTriggerStay(Collider col)
    {
        currentState.OnTriggerStay(col);
        
    }

    private void OnTriggerExit(Collider col)
    {
        currentState.OnTriggerExit(col);
        
    }

    public void Stop()
    {
        navMeshAgent.isStopped = true;
        navMeshAgent.velocity = Vector3.zero;
    }

    public void Resume()
    {
        navMeshAgent.isStopped = false;
    }

    public void BulletEnemy()
    {
        clonBulletGunEnemy = GameObject.Instantiate(bulletGunEnemy, creatorBulletGunEnemy.gameObject.transform.position, Quaternion.identity);
        Vector3 direction = creatorBulletGunEnemy.transform.forward;
        RaycastHit hit;
        if (Physics.Raycast(creatorBulletGunEnemy.transform.position, direction, out hit))
        {
            direction = hit.point - creatorBulletGunEnemy.transform.position;
            direction.y = 0;
            direction.Normalize();
        }
        clonBulletGunEnemy.GetComponent<Rigidbody>().velocity = direction * forceGunEnemy;
        
    }

}




